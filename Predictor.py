# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 14:23:44 2019

@author: Smit
"""

from keras.models import load_model
import pickle
from keras.preprocessing import image
import numpy as np
import os
import tensorflow as tf
from PIL import Image
import urllib
from GPSPhoto import gpsphoto
import pandas as pd

CLOUD_STORAGE_BUCKET = os.environ['CLOUD_STORAGE_BUCKET']

class Predictor:
    def __init__(self, category):
        if(category == 'animal'):
            self.session = tf.Session()
            self.graph = tf.get_default_graph()
            with self.graph.as_default():
                with self.session.as_default():
                    self.model = load_model('static/models/animal.h5')
            file = open('static/animal_categories.txt', 'rb')
            self.ctg = pickle.load(file)
            file.close()
        
        elif(category == 'bird'):
            self.session = tf.Session()
            self.graph = tf.get_default_graph()
            with self.graph.as_default():
                with self.session.as_default():
                    self.model = load_model('static/models/bird.h5')
            file = open('static/bird_categories.txt', 'rb')
            self.ctg = pickle.load(file)
            file.close()
        else:
            self.model = None
            self.ctg = None
    
    def predict(self, path, temp, file = True):
        if(file):
            filepath = os.path.join(temp, 'temp.jpg')
            urllib.request.urlretrieve(path, filepath)
            gps = gpsphoto.getGPSData(filepath)
            x = Image.open(filepath)
            x = x.resize((224, 224), Image.ANTIALIAS)
            x = image.img_to_array(x)
            x/=255
            x = np.expand_dims(x, axis=0)
            with self.graph.as_default():
                with self.session.as_default():
                    result = self.model.predict(x)
            
            categories = []
            y = np.argsort(result).tolist()[0][-5:][::-1]
            top1 = self.ctg[y[0]]
            top2 = self.ctg[y[1]]
            top3 = self.ctg[y[2]]
            top4 = self.ctg[y[3]]
            top5 = self.ctg[y[4]]
            top1 = ' '.join(top1.split('+')).title()
            top2 = ' '.join(top2.split('+')).title()
            top3 = ' '.join(top3.split('+')).title()
            top4 = ' '.join(top4.split('+')).title()
            top5 = ' '.join(top5.split('+')).title()
            categories.append(top1)
            df = pd.DataFrame(columns = ['Image', 'Prediction', 'Latitude', 'Longitude'])
            if('Latitude' in gps and 'Longitude' in gps):
                df = df.append({'Image': path.split('/')[-1], 'Prediction': top1, 'Latitude': gps['Latitude'], 'Longitude': gps['Longitude']}, ignore_index = True)
            else:
                df = df.append({'Image': path.split('/')[-1], 'Prediction': top1, 'Latitude': None, 'Longitude': None}, ignore_index = True)
            return list(set(categories)), [[path, [top1, round(result[:,y[0]][0], 2)], [top2, round(result[:,y[1]][0], 2)], [top3, round(result[:,y[2]][0], 2)], [top4, round(result[:,y[3]][0], 2)], [top5, round(result[:,y[4]][0], 2)]]], df
            
        else:
            results = []
            categories = []
            df = pd.DataFrame(columns = ['Image', 'Prediction', 'Latitude', 'Longitude'])
            for file in path:
                filepath = os.path.join(temp, 'temp.jpg')
                urllib.request.urlretrieve(file, filepath)
                gps = gpsphoto.getGPSData(filepath)
                x = Image.open(filepath)
                x = x.resize((224, 224), Image.ANTIALIAS)
                x = image.img_to_array(x)
                x/=255
                x = np.expand_dims(x, axis=0)
                with self.graph.as_default():
                    with self.session.as_default():
                        result = self.model.predict(x)
                y = np.argsort(result).tolist()[0][-5:][::-1]
                top1 = self.ctg[y[0]]
                top2 = self.ctg[y[1]]
                top3 = self.ctg[y[2]]
                top4 = self.ctg[y[3]]
                top5 = self.ctg[y[4]]
                top1 = ' '.join(top1.split('+')).title()
                top2 = ' '.join(top2.split('+')).title()
                top3 = ' '.join(top3.split('+')).title()
                top4 = ' '.join(top4.split('+')).title()
                top5 = ' '.join(top5.split('+')).title()
                categories.append(top1)
                if(gps):
                    df = df.append({'Image': file.split('/')[-1], 'Prediction': top1, 'Latitude': gps['Latitude'], 'Longitude': gps['Longitude']}, ignore_index = True)
                else:
                    df = df.append({'Image': path.split('/')[-1], 'Prediction': top1, 'Latitude': None, 'Longitude': None}, ignore_index = True)
                results.append([file, [top1, round(result[:,y[0]][0], 2)], [top2, round(result[:,y[1]][0], 2)], [top3, round(result[:,y[2]][0], 2)], [top4, round(result[:,y[3]][0], 2)], [top5, round(result[:,y[4]][0], 2)]])
                
            return list(set(categories)), results, df