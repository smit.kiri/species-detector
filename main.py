# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 16:05:25 2019

@author: Smit
"""

from flask import Flask, render_template, request, make_response
from Predictor import Predictor
import os
from werkzeug.utils import secure_filename
from google.cloud import storage
from random import randint, choice
import string
import pickle
import pandas as pd
import plotly
import plotly.graph_objs as go
import json

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = '/tmp'
#app.config['UPLOAD_FOLDER'] = 'C:\\Users\\Smit\\uploads'
CLOUD_STORAGE_BUCKET = os.environ['CLOUD_STORAGE_BUCKET']

@app.route('/')
@app.route('/home')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/prediction', methods = ['POST'])
def prediction():
    if(request.method == 'POST'):
        if (request.form['file_type'] == 'file'):
            try:
                file = request.files['file']
            except:
                file=None
            
            #bucket_name = '/species-detector.appspot.com'
            
            if not file:
                return render_template('index.html', label = 'No file')
                        
            usr_id = "".join(choice(string.ascii_letters) for x in range(randint(8, 14)))

            filename = secure_filename(file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filepath)
            name = '/{}/{}/{}'.format(CLOUD_STORAGE_BUCKET, usr_id, filename)
            gcs = storage.Client()
            bucket = gcs.get_bucket(CLOUD_STORAGE_BUCKET)
            blob = bucket.blob(name)
            blob.upload_from_filename(filepath)
            path = blob.public_url
            
            p = Predictor(request.form['species'])
            category, result, df = p.predict(path, temp = app.config['UPLOAD_FOLDER'], file = True, )
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], 'Predictions.csv')
            df.to_csv(filepath, sep='\t')
            name = '/{}/{}/Predictions.csv'.format(CLOUD_STORAGE_BUCKET, usr_id)
            blob = bucket.blob(name)
            blob.upload_from_filename(filepath)
            pred_csv = blob.public_url
            resp = make_response(render_template('prediction.html', result = result, category = category, pred_csv = pred_csv))
            resp.set_cookie('usr_id', usr_id)
            return resp
        
        elif(request.form['file_type'] == 'link'):
            path = request.form['file']
            
            usr_id = "".join(choice(string.ascii_letters) for x in range(randint(8, 14)))
            
            gcs = storage.Client()
            bucket = gcs.get_bucket(CLOUD_STORAGE_BUCKET)
            
            if path == '':
                return render_template('index.html', label = 'No URL entered')
            
            p = Predictor(request.form['species'])
            category, result, df = p.predict(path, temp = app.config['UPLOAD_FOLDER'], file = True)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], 'Predictions.csv')
            df.to_csv(filepath, sep='\t')
            name = '/{}/{}/Predictions.csv'.format(CLOUD_STORAGE_BUCKET, usr_id)
            blob = bucket.blob(name)
            blob.upload_from_filename(filepath)
            pred_csv = blob.public_url
            resp = make_response(render_template('prediction.html', result = result, category = category, pred_csv = pred_csv))
            resp.set_cookie('usr_id', usr_id)
            return resp
        
        else:
            files = request.files.getlist('file')
            
            if not files:
                return render_template('index.html', label = 'No folder')
            
            gcs = storage.Client()
            bucket = gcs.get_bucket(CLOUD_STORAGE_BUCKET)
            
            usr_id = "".join(choice(string.ascii_letters) for x in range(randint(8, 14)))
            
            paths = []
            for file in files:
                filename = secure_filename(file.filename)
                filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                file.save(filepath)
                name = '/{}/{}/{}'.format(CLOUD_STORAGE_BUCKET, usr_id, filename)
                
                blob = bucket.blob(name)
                blob.upload_from_filename(filepath)
                paths.append(blob.public_url)
            
            
            p = Predictor(request.form['species'])
            category, result, df = p.predict(paths, temp = app.config['UPLOAD_FOLDER'], file = False)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], 'Predictions.csv')
            df.to_csv(filepath, sep='\t')
            name = '/{}/{}/Predictions.csv'.format(CLOUD_STORAGE_BUCKET, usr_id)
            blob = bucket.blob(name)
            blob.upload_from_filename(filepath)
            pred_csv = blob.public_url
            resp = make_response(render_template('prediction.html', result = result, category = category, pred_csv = pred_csv))
            resp.set_cookie('usr_id', usr_id)
            return resp
        
@app.route('/species')
def species():
    file = open('static/animal_categories.txt', 'rb')
    animals = pickle.load(file)
    file.close()
    animals = [''.join(x.split('+')).title() for x in animals.values()]
    file = open('static/bird_categories.txt', 'rb')
    birds = pickle.load(file)
    file.close()
    birds = [''.join(x.split('_')).title() for x in birds.values()]
    return render_template('species.html', animals = animals, birds = birds)

@app.route('/map')
def map():
    usr_id = request.cookies.get('usr_id')
    name = 'http://storage.googleapis.com/species-detector.appspot.com//{}/{}/Predictions.csv'.format(CLOUD_STORAGE_BUCKET, usr_id)
    df = pd.read_csv(name, sep = '\t')
    unq_species = set(df['Prediction'])
    data = []
    for y in unq_species:
        df_sub = df[df['Prediction'] == y]
        data_sub =  go.Scattergeo(
                locationmode = 'ISO-3',
                lon = df_sub['Longitude'],
                lat = df_sub['Latitude'],
                text = df_sub['Prediction'],
                mode = 'markers',
                name = y,
                marker = dict( 
                    size = 8, 
                    opacity = 0.8,
                    reversescale = True,
                    autocolorscale = False,
                    symbol = 'circle',
                    line = dict(
                        width=1,
                        color='rgba(102, 102, 102)'
                    )
                ))
        data.append(data_sub)
    
    layout = go.Layout(
        geo = go.layout.Geo(
            resolution = 110,
            scope = 'world',
            showframe = False,
            showcoastlines = True,
            showland = True,
            landcolor = "rgb(229, 229, 229)",
            countrycolor = "rgb(255, 255, 255)" ,
            coastlinecolor = "rgb(255, 255, 255)",
            projection = go.layout.geo.Projection(
                type = 'mercator'
            ),
            lonaxis = go.layout.geo.Lonaxis(
                range= [ min(list(df['Longitude'])) - 3.0, max(list(df['Longitude'])) + 3.0 ]
            ),
            lataxis = go.layout.geo.Lataxis(
                range= [ min(list(df['Latitude'])) - 3.0, max(list(df['Latitude'])) + 3.0 ]
            ),
            domain = go.layout.geo.Domain(
                x = [ 0, 1 ],
                y = [ 0, 1 ]
            )
        ),
        geo2 = go.layout.Geo(
            scope = 'world',
            showframe = False,
            showland = True,
            landcolor = "rgb(229, 229, 229)",
            showcountries = False,
            domain = go.layout.geo.Domain(
                x = [ 0, 0.6 ],
                y = [ 0, 0.6 ]
            ),
            bgcolor = 'rgba(255, 255, 255, 0.0)',
        ),
        legend = go.layout.Legend(
               traceorder = 'reversed'
        )
    )
    
    fig = go.Figure(data=data, layout=layout)
    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    return render_template('map.html', mapped_values = graphJSON)


    
if __name__ == '__main__':
   port = int(os.environ.get('PORT', 8080))
   app.run(host='127.0.0.1', port=port, debug=True)